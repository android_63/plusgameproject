package buu.example.plusgameproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random
import kotlin.random.nextInt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        playGame()
    }
    private fun playGame() {
        val txtView1 = findViewById<TextView>(R.id.txt1)
        val txtView2 = findViewById<TextView>(R.id.txt2)
        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)
        val results = findViewById<TextView>(R.id.results)



        val number1 = Random.nextInt(0..10)
        val number2 = Random.nextInt(0..10)
        val sum = number1 + number2
        val sum2 = sum.toString()
        txtView1.text = number1.toString()
        txtView2.text = number2.toString()


        randomAns(btn1, sum, btn2, btn3)



        check(btn1, sum2, btn2, btn3)
    }

    private fun randomAns(
        btn1: Button,
        sum: Int,
        btn2: Button,
        btn3: Button
    ) {
        val ans = Random.nextInt(-1..1)
        if (ans == -1) {
            btn1.text = (sum + 0).toString()
            btn2.text = (sum + 1).toString()
            btn3.text = (sum + 2).toString()
        } else if (ans == 0) {
            btn1.text = (sum - 1).toString()
            btn2.text = (sum + 0).toString()
            btn3.text = (sum + 1).toString()
        } else if (ans == 1) {
            btn1.text = (sum - 2).toString()
            btn2.text = (sum - 1).toString()
            btn3.text = (sum + 0).toString()
        }
    }

    private fun check(
        btn1: Button,
        sum2: String,
        btn2: Button,
        btn3: Button

    ) {
        val txtAnsT = findViewById<TextView>(R.id.txtAnsT)
        val txtAnsF = findViewById<TextView>(R.id.txtAnsF)
        btn1.setOnClickListener {
            if (sum2 == btn1.text) {
                sumT(txtAnsT)
            } else {
                sumF(txtAnsF)
            }
        }
        btn2.setOnClickListener {
            if (sum2 == btn2.text) {
                sumT(txtAnsT)
            } else {
                sumF(txtAnsF)
            }
        }
        btn3.setOnClickListener {
            if (sum2 == btn3.text) {
                sumT(txtAnsT)
            } else {
                sumF(txtAnsF)
            }
        }
    }

    private fun sumF(txtAnsF: TextView) {
        Toast.makeText(this, "ไม่ถูกต้อง", Toast.LENGTH_SHORT).show()
        results.text = "ไม่ถูกต้อง".toString()
        txtAnsF.text = (txtAnsF.text.toString().toInt() + 1).toString()
    }

    private fun sumT(txtAnsT: TextView) {
        Toast.makeText(this, "ถูกต้อง", Toast.LENGTH_SHORT).show()
        results.text = "ถูกต้อง".toString()
        txtAnsT.text = (txtAnsT.text.toString().toInt() + 1).toString()
        playGame()
    }
}